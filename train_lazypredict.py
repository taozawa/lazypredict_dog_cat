# 导入必要的库
import numpy as np  # 用于处理多维数组和矩阵运算
import faiss  # 用于高效相似性搜索和稠密向量聚类
from util import createXY  # 用于创建数据集的特征和标签
from sklearn.model_selection import train_test_split  # 用于拆分数据集为训练集和测试集
import argparse  # 用于解析命令行参数
import logging  # 用于记录日志
from tqdm import tqdm  # 用于在循环中显示进度条
import time
import cv2
import pandas as pd
from sklearn.datasets import load_breast_cancer
from lazypredict.Supervised import LazyClassifier
import gradio as gr
import pickle

# 配置logging, 确保能够打印正在运行的函数名
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# 主函数，运行训练过程
def main():
    # 载入和预处理数据
    X, y = createXY(train_folder="../data/train", dest_folder=".")
    X = np.array(X).astype('float32')
    faiss.normalize_L2(X)  # 对数据进行L2归一化
    y = np.array(y)
    logging.info("数据加载和预处理完成。")

    # 数据集分割为训练集和测试集
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=2023)
    logging.info("数据集划分为训练集和测试集。")

    # 使用LazyClassifier自动选择和评估各种分类器
    clf = LazyClassifier()
    result, _ = clf.fit(X_train, X_test, y_train, y_test)
    print(result)

    # 获取Accuracy最高的模型
    best_model_name = result['Accuracy'].idxmax()  # 获取Accuracy最高行的索引值，即：模型名称
    best_accuracy = result.loc[best_model_name, 'Accuracy']   #获取准确率最高的准确率值
    print(f"\n准确率最高的模型是:{best_model_name},准确率为:{best_accuracy}")

    # clf.models 是包含所有训练过的模型 (名称, 模型对象) 键值对的字典
    best_model = clf.models[best_model_name]  # 根据模型名称，从模型字典中获取模型对象 
    with open('best_model.pkl', 'wb') as f:
        pickle.dump(best_model, f)     # 序列化best_model并写入文件
    result = best_model.predict(X_test)  # 该字典可以直接被拿来进行预测  
    print(f"用{best_model_name}预测X_test的结果是:\n{result}")
    
# 如果是主脚本，则执行main函数
if __name__ == '__main__':
    main()