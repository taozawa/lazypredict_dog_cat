# 项目描述
本项目使用FAISS库实现了基于K近邻的图像分类器。该分类器可以使用CPU或GPU进行训练，并支持两种特征提取方法：flat和vgg。用户可以选择使用sklearn或faiss库实现K近邻算法。

# 功能
本项目实现了以下功能：
- 使用FAISS库实现基于K近邻的图像分类器
- 支持使用CPU或GPU进行训练
- 支持两种特征提取方法：flat和vgg
- 支持使用sklearn或faiss库实现K近邻算法

# 依赖
本项目依赖以下库：
- numpy
- faiss
- sklearn
- argparse
- logging
- tqdm
- cv2
- os
- imutils
- pickle
- tensorflow

# 使用
1. 安装依赖库
2. 运行训练脚本
```bash
python train.py -m [cpu|gpu] -f [flat|vgg] -l [sklearn|faiss]
```
其中：
   - `-m`：选择训练模式，可选值为cpu或gpu
   - `-f`：选择特征提取方法，可选值为flat或vgg
   - `-l`：选择使用的库，可选值为sklearn或faiss

3. 查看训练结果
- 训练完成后，程序会输出最佳的k值和相应的准确率。

# 项目运行效果截图
![输入图片说明](%E6%95%88%E6%9E%9C%E6%88%AA%E5%9B%BE/%E8%AE%AD%E7%BB%83%E6%A8%A1%E5%9E%8B.jpg)
![输入图片说明](%E6%95%88%E6%9E%9C%E6%88%AA%E5%9B%BE/%E5%AE%9E%E6%93%8D%E6%88%AA%E5%9B%BE.png)
注意
- 本项目使用Python 3.8及以上版本进行开发和测试。
- 本项目使用FAISS库进行高效的相似度搜索和稠密向量的聚类。
- 本项目使用sklearn库中的K近邻分类器作为对比实验。
- 本项目使用VGG16模型进行图像特征提取。
- 本项目使用logging库记录日志。
- 本项目使用tqdm库在循环中添加进度条。

## 个人信息
- 学号: 202152320123
- 年级: 2021
- 专业: 智能科学与技术
- 班级: 1班